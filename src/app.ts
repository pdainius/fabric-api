import express from "express";
import config from "./config";
import loaders from "./loaders";



async function startServer() {
    const app = express();

    try {
        await loaders({ expressApp: app });
    } catch (err) {
        console.log("ERROR | failed to initialize application: ", err);
        process.exit(1);
    }

    app.listen(config.port, (): void => {
        console.log(`
          ################################################
              Server listening on port: ${config.port}    
          ################################################
        `);
    });
}
startServer();
