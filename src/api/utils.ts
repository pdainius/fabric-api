import express from "express";

export const asyncRoute = (fn: Function) => (req: any, res: any, next: express.NextFunction) => {
    Promise.resolve(fn(req, res, next)).catch(next);
};