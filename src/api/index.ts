import express from "express";
const router = express.Router();

import fabricCA from './routes/fabricCA';


export default () => {
    fabricCA(router);
    return router;
};
