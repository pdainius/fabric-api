import FabricCAServices from 'fabric-ca-client';
import { Wallet, Wallets } from 'fabric-network';
import { User } from "fabric-common";
import config from "../../config";
import { CSR } from "../../models/csr";
import * as asn1js from "asn1js";
import { CertificationRequest } from "pkijs";
import { stringToArrayBuffer, fromBase64 } from "pvutils";

var _wallet: Wallet;
var _adminUser: User;

const getWallet = async (): Promise<Wallet> => {
    if (!_wallet) {
        _wallet = await Wallets.newFileSystemWallet(config.walletPath);
    }
    return _wallet;
}

const getAdminUser = async (): Promise<User> => {
    if (!_adminUser) {
        var wallet = await getWallet();
        const adminIdentity = await wallet.get('admin');
        if (!adminIdentity) {
            throw Error("admin identity not found in a wallet");
        }
        const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type);
        _adminUser = await provider.getUserContext(adminIdentity, 'admin');
    }
    return _adminUser;
}

const enroll = async (req: FabricCAServices.IEnrollmentRequest): Promise<any> => {
    const enrollment = await config.ca.enroll(req);
    return {
        credentials: {
            certificate: enrollment.certificate
        },
        mspId: config.txMspid,
        type: "X.509"
    }
}

const register = async (req: FabricCAServices.IRegisterRequest): Promise<string> => {
    const adminUser = await getAdminUser();
    return await config.ca.register(req, adminUser);
}

const parseCSR = async (csr: string): Promise<CSR> => {
    const csrObject = (): CSR => ({
        C: '',
        OU: '',
        O: '',
        CN: '',
        L: '',
        ST: '',
        T: '',
        GN: '',
        I: '',
        SN: '',
    });
    const typemap = {
        "2.5.4.6": "C",
        "2.5.4.11": "OU",
        "2.5.4.10": "O",
        "2.5.4.3": "CN",
        "2.5.4.7": "L",
        "2.5.4.8": "ST",
        "2.5.4.12": "T",
        "2.5.4.42": "GN",
        "2.5.4.43": "I",
        "2.5.4.4": "SN"
    };

    const stringPEM = csr.replace(/(-----(BEGIN|END) CERTIFICATE REQUEST-----|\n)/g, "");
    const asn1 = asn1js.fromBER(stringToArrayBuffer(fromBase64((stringPEM))));

    const pkcs10 = new CertificationRequest({ schema: asn1.result })

    for(let i = 0; i < pkcs10.subject.typesAndValues.length; i++)
    {
        let typeval = typemap[pkcs10.subject.typesAndValues[i].type];
        if(typeof typeval === "undefined")
            typeval = pkcs10.subject.typesAndValues[i].type;
        
        const subjval = pkcs10.subject.typesAndValues[i].value.valueBlock.value;

        switch(typeval) {
            case "C": { csrObject.C = subjval; break; }
            case "OU": { csrObject.OU = subjval; break; }
            case "O": { csrObject.O = subjval; break; }
            case "CN": { csrObject.CN = subjval; break; }
            case "L": {csrObject.L = subjval; break;}
            case "ST": { csrObject.ST = subjval; break; }
            case "T": { csrObject.T = subjval; break; }
            case "GN": { csrObject.GN = subjval; break;}
            case "I": { csrObject.I = subjval; break;}
            case "SN": { csrObject.SN = subjval; break;}
        }
    }

    return csrObject;
}

export {
    enroll,
    register,
    getAdminUser,
    getWallet,
    parseCSR
}
