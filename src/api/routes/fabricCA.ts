import { Response, Router } from "express";
import { asyncRoute } from "../utils";
import { enroll, register, parseCSR } from "../services/hlfService";
import Joi from "@hapi/joi";

const schema = Joi.object({
    csr: Joi.string().required()
});


export default (app: Router) => {
    app
        .route("/certificate/enroll")
        .post(asyncRoute(async (req: Request<{ schema }>, res: Response) => {
            const validationResult = schema.validate(req.body);
            if (validationResult.error) {
                res.status(400).json(validationResult.error)
            }

            try {
                const csrObject = await parseCSR(req.body['csr']);

                const enrollmentSecret = await register({
                    affiliation: "",
                    enrollmentID: csrObject.CN,
                    role: "client",
                    attrs: [
                        {
                            name: "bakalauras.id",
                            value: csrObject.CN,
                            ecert: true
                        }
                    ]

                });

                const response = await enroll({
                    enrollmentID: csrObject.CN,
                    enrollmentSecret,
                    csr: req.body['csr']
                })

                res.json(response)
            } catch (err) {
                if (err.response) {
                    res.status(422).json(err.response.data)
                } else {
                    res.status(422).json({ message: err.message })
                }
            }
        }));
};

export interface Request<T> {
    body: T
}