import FabricCAServices from 'fabric-ca-client';
import * as fs from 'fs';
import { ConnectionProfile } from '../models/connectionProfile';

var env = process.env.NODE_ENV || 'dev';

const dotenvConfig = (() => {
  if (fs.existsSync('../.env')) {
    return { path: '../.env' }
  }

  return {}
})()

if (env === 'dev') {
  require('dotenv').config(dotenvConfig);
} else {
  console.log("NODE_ENV: " + env);
  console.log("Use 'dev' to load environment vars from '.env' file");
}

const {
  PORT,
  WALLET_PATH,
  TX_DOMAIN,
  TX_CONNECTION_PROFILE_PATH,
  TX_DISCOVERY_ENABLED,
  TX_DISCOVERY_ASLOCALHOST,
  CA_ADMIN_ENROLLMENT_ID,
  CA_ADMIN_ENROLLMENT_SECRET,
  ENROLL_CA_ADMIN,
}: any = process.env;

var ccp: ConnectionProfile, ca: FabricCAServices, mspId: string, caInfo: any;
try {
  ccp = JSON.parse(fs.readFileSync(TX_CONNECTION_PROFILE_PATH, 'utf8'));
  mspId = ccp.organizations[`ktu.${TX_DOMAIN}`].mspid;
  caInfo = ccp.certificateAuthorities[`ca.ktu.${TX_DOMAIN}`];
  ca = new FabricCAServices(caInfo.url, { trustedRoots: caInfo.tlsCACerts.pem, verify: false }, caInfo.caName);
} catch (err) {
  console.log('ERROR | Failed to read connection profile: ', err);
  process.exit(1);
}

export default {
  enrollCaAdmin: ENROLL_CA_ADMIN,
  ccp: ccp,
  ca: ca,
  caInfo: caInfo,
  walletPath: WALLET_PATH || "/usr/src/app/wallet",
  port: parseInt(PORT || 8080, 10),
  txMspid: mspId,
  txConnectionProfilePath: TX_CONNECTION_PROFILE_PATH,
  txDiscovery: {
    enabled: TX_DISCOVERY_ENABLED || true,
    asLocalhost: TX_DISCOVERY_ASLOCALHOST || false,
  },
  caAdminEnrollmentId: CA_ADMIN_ENROLLMENT_ID || 'admin',
  caAdminEnrollmentSecret: CA_ADMIN_ENROLLMENT_SECRET || 'adminpw'
}
