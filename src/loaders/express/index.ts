import routes from "../../api";
import express, { Application } from "express";

export default async (app: Application) => {
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    app.use("/api/v1", routes());
};
