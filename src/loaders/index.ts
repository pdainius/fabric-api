import { Application } from "express";
import expressLoader from "./express";
import walletLoader from "./wallet";

export default async (params: { expressApp: Application }) => {
    await walletLoader();
    console.log('--> Wallet with admin identity loaded');

    await expressLoader(params.expressApp);
    console.log('--> Express loaded');

}
