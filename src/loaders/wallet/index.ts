import { Wallets, X509Identity } from 'fabric-network';
import config from "../../config";



export default async () => {
    console.log('Loading wallet...');
    var wallet = await Wallets.newFileSystemWallet(config.walletPath);
    let adminIdentity = await wallet.get('admin');
    if (!adminIdentity) {
        console.log('No admin identity found in wallet.');
        console.log(config);
        if (config.enrollCaAdmin === "false") {
            throw new Error("no admin identity found and enrollment turned off");
        }

        const enrollment = await config.ca.enroll({ enrollmentID: config.caAdminEnrollmentId, enrollmentSecret: config.caAdminEnrollmentSecret });
        console.log('Admin successfully enrolled.');
        const identity: X509Identity = {
            credentials: {
                certificate: enrollment.certificate,
                privateKey: enrollment.key.toBytes(),
            },
            mspId: config.txMspid,
            type: 'X.509',
        };
        await wallet.put('admin', identity);
        console.log('Admin identity saved to wallet');
    }

}
