export interface RevokeCSR {
    enrollmentID: string;
    aki: string;
    serial: string;
    reason: string;
    gencrl: boolean;
    authorizationToken: string;
}
