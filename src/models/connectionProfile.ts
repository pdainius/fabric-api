export interface Peer {
    endorser: string;
}

export interface Timeout {
    peer: Peer;
}

export interface Connection {
    timeout: Timeout;
}

export interface Client {
    organization: string;
    connection: Connection;
}

export interface Organization {
    mspid: string;
    peers: string[];
    certificateAuthorities: string[];
}

export interface Organizations {
    [key: string]: Organization;
}

export interface PeerTlsCACerts {
    pem: string;
}

export interface GrpcOptions {
    [option: string]: string
}

export interface Peer {
    url: string;
    tlsCACerts: PeerTlsCACerts;
    grpcOptions: GrpcOptions;
}

export interface Peers {
    [key: string]: Peer;
}

export interface CATlsCACerts {
    pem: string[];
}

export interface HttpOptions {
    verify: boolean;
}

export interface CertificateAuthority {
    url: string;
    caName: string;
    tlsCACerts: CATlsCACerts;
    httpOptions: HttpOptions;
}

export interface CertificateAuthorities {
    [key: string]: CertificateAuthority;
}

export interface ConnectionProfile {
    name: string;
    version: string;
    client: Client;
    organizations: Organizations;
    peers: Peers;
    certificateAuthorities: CertificateAuthorities;
}
