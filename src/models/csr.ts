export interface CSR {
    C: string;
    OU: string;
    O: string;
    CN: string;
    L: string;
    ST: string;
    T: string;
    GN: string;
    I: string;
    SN: string;
}