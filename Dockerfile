FROM node:12-alpine as BUILD_IMAGE
RUN apk update && apk add python make g++
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:12-alpine
WORKDIR /usr/src/app

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
RUN update-ca-certificates

COPY --from=BUILD_IMAGE /usr/src/app/dist .
COPY --from=BUILD_IMAGE /usr/src/app/node_modules ./node_modules
RUN ls -la

ENV NODE_TLS_REJECT_UNAUTHORIZED=0
EXPOSE 8080

CMD [ "node", "app.js" ]
